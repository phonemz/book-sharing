// src/app/borrow-book/borrow-book.component.ts

import { Component } from '@angular/core';
import { DataSharingService } from '../data-sharing.service';

@Component({
  selector: 'app-borrow-book',
  templateUrl: './borrow-book.component.html',
  styleUrls: ['./borrow-book.component.css'],
})
export class BorrowBookComponent {
  // Assuming you have a book object, adjust as needed
  bookToBorrow = {
    title: 'The Book Title',
    author: 'Author Name',
    // Add more book details as needed
  };

  constructor(private dataSharingService: DataSharingService) {}

  borrowBook() {
    // Save the borrowed book data to the service
    this.dataSharingService.borrowedBookData = this.bookToBorrow;
  }
}
