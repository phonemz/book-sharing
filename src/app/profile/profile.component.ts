// src/app/profile/profile.component.ts

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  editing = false;
  editedName: string = '';
  editedEmail: string = '';

  constructor() { }

  ngOnInit(): void {
    // Set initial values for editedName and editedEmail
    this.editedName = 'John Doe';
    this.editedEmail = 'john.doe@example.com';
  }

  editProfile() {
    // Enable editing mode
    this.editing = true;
  }

  saveProfile() {
    // Save edited profile details
    // For simplicity, we'll just update the displayed name and email
    this.editedName = this.editedName;
    this.editedEmail = this.editedEmail;

    // Disable editing mode
    this.editing = false;
  }

  deleteProfile() {
    // Implement delete profile functionality
    // For simplicity, we'll just clear the form
    this.clearForm();
  }

  clearForm() {
    // Clear the form fields
    this.editedName = '';
    this.editedEmail = '';

    // Disable editing mode
    this.editing = false;
  }

  updateProfile() {
    // Implement update profile functionality
    // For simplicity, we'll just save the changes
    this.saveProfile();
  }

  logout() {
    // Implement logout functionality
    // Redirect to the login page or perform any other necessary actions
    console.log('Logout clicked');
  }
}
