import { Component } from '@angular/core';
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css'],
  providers: [UserService],
})
export class RegistrationComponent {
  user = {
    username: '',
    email: '',
    password: '',
  };

  constructor(private userService: UserService, private router: Router) {}

  onSubmit() {
    
        this.router.navigate(['/login']);
        // Add any additional logic or redirection here

  }
}
